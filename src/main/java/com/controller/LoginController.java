package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.business.LoginService;
import com.model.User;

@RestController
public class LoginController {

	@Autowired
	LoginService loginService;

	@RequestMapping(value = "/Login", method = RequestMethod.POST)
	public User userLogin(@RequestBody User user) {

		User userDB = loginService.getUserDetails(user);

		return userDB;
	}

}
