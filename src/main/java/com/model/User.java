package com.model;

import java.io.Serializable;

public class User implements Serializable{
	
	private String username;
	private String userpassword;
	private String userFullName;
	
	public User(String username, String userpassword, String userfullname) {
		super();
		this.username = username;
		this.userpassword = userpassword;
		this.userFullName = userfullname;
	}

	public User() {
		
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserpassword() {
		return userpassword;
	}

	public void setUserpassword(String userpassword) {
		this.userpassword = userpassword;
	}

	public String getUserFullName() {
		return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}
	
	
	
}
