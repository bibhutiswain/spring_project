package com.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import com.model.User;

@Repository
public class ConnectMysql {

	public User getFromDB(User user) {
		ResultSet rs = null;
		User userdb = null;
		PreparedStatement ps = null;
		Connection connect = null;

		String sql = "SELECT * FROM user_details WHERE USER_NAME = ? AND USER_PWD = ?";

		try {
			String driverName = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost:3306/mysql";
			Class.forName(driverName);
			connect = DriverManager.getConnection(url, "root", "root");

			ps = connect.prepareStatement(sql);
			ps.setString(1, user.getUsername());
			ps.setString(2, user.getUserpassword());

			rs = ps.executeQuery();
			if (rs.next()) {
				userdb = new User();
				userdb.setUserFullName(rs.getString("USER_FULL_NAME"));
				userdb.setUsername(rs.getString("USER_NAME"));
				userdb.setUserpassword(rs.getString("USER_PWD"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (ps != null) {
				try {
					ps.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

		return userdb;

	}
}
