package com.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dao.LoginDao;
import com.model.User;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	LoginDao loginDao;

	public User getUserDetails(User user) {
		User userDB = null ;

		if (validateUser(user)) {
			userDB = loginDao.getUserDetails(user);
		}

		return userDB;
	}

	private boolean validateUser(User user) {

		boolean validated = false;
		if (null != user) {
			if (null != user.getUsername()|| null!= user.getUserpassword()) {
				validated = true;
			}
		}
		return validated;
	}
}
