package com.business;

import com.model.User;

public interface LoginService {

	public User getUserDetails(User user);
}
