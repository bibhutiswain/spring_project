package com.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.User;
import com.utility.ConnectMysql;

@Repository
public class LoginDaoImpl implements LoginDao {

	@Autowired
	ConnectMysql connectMysql;

	@Override
	public User getUserDetails(User user) {

		User userdb = connectMysql.getFromDB(user);

		return userdb;
	}

}
